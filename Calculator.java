public class Calculator
{
	public static int add (int term1, int term2)
	{
		int sum = term1+term2;
		return sum;
	}
	
	public static int substract (int term1, int term2)
	{
		int difference = term1-term2;
		return difference;
	}
	
	public int multiply (int term1, int term2)
	{
		int product = term1*term2;
		return product;
	}
	
	public double divide (double term1, double term2)
	{
		double quotient = term1/term2;
		return quotient;
	}
}