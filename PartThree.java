import java.util.Scanner;
public class PartThree
{
	public static void main (String[] args)
	{
		Scanner scan = new Scanner(System.in);
		int term1 = scan.nextInt();
		int term2 = scan.nextInt();
		
		System.out.println(Calculator.add(term1, term2));
		System.out.println(Calculator.substract(term1, term2));
		
		Calculator math = new Calculator();
		System.out.println(math.multiply(term1, term2));
		System.out.println(math.divide(term1, term2));
	}
}