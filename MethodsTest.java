public class MethodsTest
{
	public static void main (String[] args)
	{
		int x = 5;
		System.out.println(x);
		methodOneInputNoReturn(x+10);
		System.out.println(x);
		
		double y = 10.5;
		methodTwoInputNoReturn(x,y);
		
		int z = methodNoInputReturnInt();
		System.out.println(z);
		
		int term1 = 9;
		int term2 = 5;
		double sqrt = sumSquareRoot(term1, term2);
		System.out.println(sqrt);
		
		String s1 = "java";
		String s2 = "programming";
		System.out.println(s1.length());
		System.out.println(s2.length());
		
		System.out.println(SecondClass.addOne(50));
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
		
	}
	
	public static void methodOneInputNoReturn(int num)
	{
		System.out.println("Inside the method one input no return");
		num = num-5;
		System.out.println(num);
	}
	
	public static void methodTwoInputNoReturn(int num1, double num2)
	{
		System.out.println(num1);
		System.out.println(num2);
	}
	
	public static int methodNoInputReturnInt()
	{
		return 5;
	}
	
	public static double sumSquareRoot(int num1, int num2)
	{
		int sum = num1+num2;
		double squareRoot = Math.sqrt(sum);
		return squareRoot;
	}
}
	